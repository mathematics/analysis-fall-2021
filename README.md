# Математический анализ &mdash; 1 | осень 2021



## Команда курса

- Антон Бойцев [[email](boitsevanton@itmo.ru)]
- Алексей Фотин  [[telegram](https://t.me/alexsei14)]
- Иосиф Гордон [[telegram](https://t.me/joseph_gordon)]


[Видеозаписи лекций и практик (плейлист)](https://www.youtube.com/watch?v=9TdPaAf70cU&list=PL4PvyDyhNGsnbEjabSXNjDDcAJAm_X1y-)

# Лекция 1

- Логическая символика: единственность, импликация, равносильность, принадлежность, тождественность, логические И и ИЛИ, отрицание, "рассмотрим", "пусть", "такое,что", кванторы всеобщности и существования. 
- Множества и операции над ними: способы задания,равные множества, пустое, объединение, пересечение,разность множеств, законы де Моргана, универсальное множество, свойства, операции, декартово произведение 


# Лекция 2.
-	Функции и отображения : область определения, область значений, свойства.
-	Образ и прообраз, инъекция,сюръекция и биекция, обратное отображение, композиция

# Лекция 3

- Множество вещественных чисел: аксиомы сложения, умножения, связи между ними, аксиомы порядка, связи сложения и порядка, умножения и порядка, аксиомы непрерывности 
-	Следствия из аксиом множества вещественных чисел: доказательство этих следствий
-	Расширение множества вещественных чисел: операции сложения, умножения вещественного числа на +- бесконечность, сложение и умножение бесконечностей, неопределенности
# Лекция 4
- Натуральные числа. Принцип мат.индукции: индуктивное множество, множество натуральных чисел,неравенство Бернулли
- Бином Ньютона (доказано), факториал, число сочетаний
- Целые, рациональные и иррациональные числа; доказательство иррациональности $`\sqrt2`$.
	
	
	


